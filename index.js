
let posts = [];


let count = 1;


const formAddPost = document.querySelector('#form-add-post')
formAddPost.addEventListener('submit', (event)=> {
    let title = document.querySelector('#txt-title').value;
    let body = document.querySelector('#txt-body').value;
    event.preventDefault();


    let newPost = {
        id: count ++,
        title: title,
        body: body
    }

    
    posts.push(newPost);

    showPosts(posts)

    document.querySelector('#txt-title').value = '';
    document.querySelector('#txt-body').value = '';


})

const showPosts = (posts)=>{

   
    // posts.reverse();// code for the last will be go on top

    let postEntries= ``;

    posts.forEach( (item,index)=> {
        postEntries += ` <div id = "post-${item.id}" > 
        <h3 id="post-title-${item.id}" > ${item.title}</h3>
        <p id="post-body-${item.id}">${item.body} </p>
        <button onClick = "editPost(${item.id})">Edit</button>
        <button onClick = "deletePost(${index}, ${item.id})">Delete</button>
        </div>`
    } )

    document.querySelector('#div-post-entries').innerHTML = postEntries;

}


const editPost = (id)=>{
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;
}



document.querySelector('#form-edit-post').addEventListener('submit', (event)=> {
    event.preventDefault();

    posts.forEach( (item=>{
        let idTobeEdited = document.querySelector('#txt-edit-id').value;

        if(item.id == idTobeEdited){
            let title = document.querySelector('#txt-edit-title').value;
            let body = document.querySelector('#txt-edit-body').value;

            posts[item.id-1].title = title;
            posts[item.id-1].body = body;

            alert('edit is success!');

            document.querySelector('#txt-edit-title').value = '';
            document.querySelector('#txt-edit-body').value = '';

            showPosts(posts);
        }
    }))
})



/// delete post

const deletePost = (index, id)=>{
   let itemDelete = document.querySelector(`#post-${id}`);
   itemDelete.remove();

    

   console.log(index);
   posts.splice(index, 1)
   showPosts(posts);

}